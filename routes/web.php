<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test/{angka}', function ($angka) {
    return view('test', ["angka" => $angka]);
});

Route::get('/halo/{nama}', function ($nama) {
    return "Haloo $nama";
});

Route::get('/form' , 'RegisterController@form');

Route::get('/sapa', 'RegisterController@sapa');
Route::post('/sapa', 'RegisterController@sapa_post');

Route::get('/home', 'HomeController@home');

Route::get('/regist', 'AuthController@regist');
Route::post('/welkam', 'AuthController@welkam_post');


Route::get('/master', function () {
    return view('layoutadm.master');
});

Route::get('/items', function () {
    return view('items.index');
});

Route::get('/items/create', function () {
    return view('items.create');
});


Route::get('/table', function () {
    return view('table');
});

Route::get('/data-table', function () {
    return view('data-table');
});

