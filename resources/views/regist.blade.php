<!DOCTYPE html>
<html lang="en">
<head>
	<title>Sign Up</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h2>Sign Up Form</h2> 
	<form action="/welkam" method="POST">
		{{ csrf_field() }}
		<label for="nama">First Name :</label> <br><br>
		<input type="text" name="nama">
		<br><br>
		<label for="nama2">Last Name:</label><br><br>
		<input type="text" name="nama2">
		<br><br>
		<label for="gender">Gender :</label><br><br>
		<input type="radio" name="gender"> Male <br>
		<input type="radio" name="gender"> Female <br>
		<input type="radio" name="gender"> Other <br>
		<br>
		<label>Nationality :</label><br><br>
		<select name="Nat">
			<option value="Ind">Indonesian</option>
			<option value="Sg">Singaporean</option>
			<option value="Sg">Malaysian</option>
			<option value="Sg">Australian</option>
		</select>
		<br><br>
		<label> Language Spoken :</label><br><br>
		<input type="checkbox"> Bahasa Indonesia<br>
		<input type="checkbox"> English<br>
		<input type="checkbox"> Other <br>
		<br>
		<label>Bio :</label>
		<br><br>
		<textarea name="alamat" cols="30" rows="10"></textarea>
		<br><br>
		<button>Sign Up</button>
	</form>

</body>
</html>